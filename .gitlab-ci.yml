image: buildstream/buildstream-fedora:master-113-499df6a5

variables:
  # Store everything under the /builds directory. This is a separate Docker
  # volume. Note that GitLab CI will only cache stuff inside the build
  # directory.
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  GET_SOURCES_ATTEMPTS: 3
  BST_CACHE_SERVER_ADDRESS: 'testcache.codethink.co.uk'
  BST_RELEASES_SERVER_ADDRESS: 'cache.sdk.freedesktop.org'
  RUNTIME_VERSION: '18.08'

  # Generic variable for invoking buildstream
  BST: bst --colors

stages:
  - flatpak
  - vm
  - publish_x86_64
  - publish_i586
  - publish_aarch64
  - publish_arm

before_script:
  - export PATH=~/.local/bin:${PATH}
  - export BST_SHA='70971f24dfe09fe89a233f22b8d6034a64d11167' #  1.2.0
  - git clone https://gitlab.com/BuildStream/buildstream.git
  - git -C buildstream checkout $BST_SHA
  - pip3 install --user buildstream/
  - export BST_EXTERNAL_SHA='f5bb2ac9d511a93c8709c32b70d24d3a57a0c15c' #  0.4
  - git clone https://gitlab.com/BuildStream/bst-external.git
  - git -C bst-external checkout $BST_EXTERNAL_SHA
  - pip3 install --user ./bst-external

  # Create ~/.ssh for storing keys
  - mkdir -p ~/.ssh

  # Private key stored as a protected variable that allows pushing to
  # cache.sdk.freedesktop.org
  - |
    if [ -z "$freedesktop_ostree_cache_private_key" ]; then
        echo >&2 "Private key for cache.sdk.freedesktop.org is not available."
    else
        echo "$freedesktop_ostree_cache_private_key" > ~/.ssh/id_rsa
        chmod 600 ~/.ssh/id_rsa
        ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub
    fi

  # Create CAS directory for SSL keys
  - mkdir -p /etc/ssl/CAS
 
  # Private SSL keys/certs for psuhing to the CAS server
  - |
    if [ -z "$GITLAB_CAS_PUSH_CERT" ]; then
       echo >&2 "Private CAS cert not found"
    else
       echo "$GITLAB_CAS_PUSH_CERT" > /etc/ssl/CAS/server.crt
       echo "$GITLAB_CAS_PUSH_KEY" > /etc/ssl/CAS/server.key
    fi

  # If we can push, then enable push and pull for freedesktop-sdk artifact cache
  # (default config is pull only)
  - |
    if [ -n "$freedesktop_ostree_cache_private_key" ]; then
        mkdir -p ~/.config
        echo "projects:" > ~/.config/buildstream.conf
        echo "  freedesktop-sdk-bootstrap:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        echo "      url: https://${BST_CACHE_SERVER_ADDRESS}:11002" >> ~/.config/buildstream.conf
        echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
        echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
        echo "  freedesktop-sdk:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        echo "      url: https://${BST_CACHE_SERVER_ADDRESS}:11002" >> ~/.config/buildstream.conf
        echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
        echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
    fi

# Store all the downloaded git and ostree repos in the distributed cache.
# This saves us fetching them on every build
.gitlab_cache_template_pull: &gitlab_cache_pull
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"
    policy: pull

.gitlab_cache_template_pull_push: &gitlab_cache_pull_push
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"



.flatpak_template: &flatpak_definition
  stage: flatpak
  script:
    - make ARCH=${ARCH} build
    - make ARCH=${ARCH} check-dev-files

    - dnf install -y flatpak flatpak-builder --enablerepo=updates-testing
    - export FLATPAK_USER_DIR="${PWD}/tmp-flatpak"
    - make ARCH=${ARCH} export
    - make ARCH=${ARCH} test-apps
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '18.08'
  <<: *gitlab_cache_pull

app_x86_64:
  <<: *flatpak_definition
  tags:
    - x86_64
  variables:
    ARCH: x86_64

app_i586:
  <<: *flatpak_definition
  tags:
    - x86_64
  variables:
    ARCH: i586

app_aarch64:
  image: buildstream/buildstream-fedora:aarch64-master-113-499df6a5
  <<: *flatpak_definition
  tags:
    - aarch64
  variables:
    ARCH: aarch64

app_arm:
  image: buildstream/buildstream-fedora:aarch64-master-113-499df6a5
  <<: *flatpak_definition
  tags:
    - armhf
  variables:
    ARCH: arm


.vm_image_template: &vm_image
  stage: vm
  script:
    - ${BST} -o target_arch "${ARCH}" build vm/"${TYPE}"-vm-image-"${ARCH}".bst
    - ${BST} -o target_arch "${ARCH}" checkout vm/"${TYPE}"-vm-image-"${ARCH}".bst ./vm
    - dnf install -y qemu-system-x86
    - utils/test-minimal-system --dialog "${DIALOG}" vm/sda.img
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '18.08'
  <<: *gitlab_cache_pull

minimal_vm_image_x86_64:
  tags:
    - x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal
    DIALOG: minimal

minimal_systemd_vm_image_x86_64:
  tags:
    - x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal-systemd
    DIALOG: root-login

.flatpak_runtimes_publish_template: &flatpak_runtimes_publish
  script:
    - |
      dnf install -y gpg

    - |
      [ -n "${FREEDESKTOP_GPG_PRIV}" ]
    - |
      echo "${FREEDESKTOP_GPG_PRIV}" | base64 --decode | gpg --import

    - make ARCH=${ARCH} build

    - echo "Clone the releases OSTree repo locally"
    - ostree init --repo=releases --mode=archive-z2
    - ostree remote add --repo=releases origin "https://${BST_RELEASES_SERVER_ADDRESS}/releases/" --no-gpg-verify
    - ostree pull --repo=releases origin --mirror

    - dnf install -y flatpak --enablerepo=updates-testing
    - make ARCH=${ARCH} GPG_OPTS="--gpg-sign=${FREEDESKTOP_GPG_ID}" REPO=releases export

    - echo "Push to the releases ostree repo"
    - export OSTREE_PUSH_SHA='9aa82b67325786a810653155b952a17b7ccc436a'
    - git clone https://github.com/ssssam/ostree-push.git
    - git -C ostree-push/ checkout "${OSTREE_PUSH_SHA}"
    # Trust the host key of the release server.
    - ssh-keyscan "${BST_RELEASES_SERVER_ADDRESS}" >> ~/.ssh/known_hosts
    - ostree-push/ostree-push --repo=releases ssh://releases@"${BST_RELEASES_SERVER_ADDRESS}"
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
    - master
    - '18.08'
  <<: *gitlab_cache_pull_push

publish_x86_64:
  stage: publish_x86_64
  <<: *flatpak_runtimes_publish
  tags:
    - x86_64
  variables:
    ARCH: x86_64

publish_i586:
  stage: publish_i586
  <<: *flatpak_runtimes_publish
  tags:
    - x86_64
  variables:
    ARCH: i586

publish_aarch64:
  stage: publish_aarch64
  image: buildstream/buildstream-fedora:aarch64-master-113-499df6a5
  <<: *flatpak_runtimes_publish
  tags:
    - aarch64
  variables:
    ARCH: aarch64

publish_arm:
  stage: publish_arm
  image: buildstream/buildstream-fedora:aarch64-master-113-499df6a5
  <<: *flatpak_runtimes_publish
  tags:
    - armhf
  variables:
    ARCH: arm
